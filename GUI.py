import sys
from PyQt5.QtWidgets import *
import dataset.Generation_dataset as GD
import loading_dataset as ld
from PyQt5.QtGui import QPixmap
from PIL import Image
import struct
import Interpreter


class Main(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        Generation_Dataset = QPushButton("Сгенерировать набор данных ")

        self.Grid = QGridLayout(self)
        self.Grid.addWidget(Generation_Dataset, 0, 0)


        self.Code = QTextEdit('')
        self.Code.setMinimumSize(200, 100)
        self.Code_Text = ''
        self.Code.setText(self.Code_Text)
        self.Grid.addWidget(self.Code, 0, 2, 3, 2)


        self.List_Data = QListWidget()
        self.Grid.addWidget(self.List_Data, 1, 0)
        self.But_Open = QPushButton('Показать макет и код')
        self.Grid.addWidget(self.But_Open, 2, 0)

        self.setGeometry(300, 300, 300, 150)
        self.setWindowTitle('Generator')
        self.show()

        Generation_Dataset.clicked.connect(self.Generation_data)
        self.List_Data.itemClicked.connect(self.Current_Item)
        self.But_Open.clicked.connect(self.Open_L_C)

        self.Generation_data()


    def Generation_data(self):
        GD.Generation()
        print('Сгенерированно')
        self.List_Data.clear()
        for i in range(1000):
            self.List_Data.addItem('Макет ' + str(i+1))

        self.Data = ld.load()



    def Current_Item(self):
        self.Current = self.List_Data.currentRow()
        # print(self.Current)


    def Open_L_C(self):
        size = 100, 100

        arr = self.Data[self.Current][0]*255
        # print(arr)
        data = struct.pack('B'*len(arr), *[int(pixel) for pixel in arr])
        img = Image.frombuffer('L', size, data, 'raw', 'L', 0, 1)
        img.save('image.png')

        label = QLabel(self)
        pixmap = QPixmap('image.png')
        label.setPixmap(pixmap)
        self.Grid.addWidget(label, 0, 1, 0, 1)


        self.Code_Text = Interpreter.BinCode_to_HtmlCode(self.Data[self.Current][1])
        self.Code.setText(self.Code_Text)



if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Main()
    sys.exit(app.exec_())



