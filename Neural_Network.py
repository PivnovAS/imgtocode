import numpy as np
from keras.datasets import cifar10
import loading_dataset as ld
from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation
from keras.layers import Dropout
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.optimizers import SGD


np.random.seed(42)


# (X_train, y_train), (X_test, y_test) = cifar10.load_data()
# # X_train1 = X_train1.astype('float32')
# print(X_train[0])
#
Data = ld.load()

# X_train = Data[:800, 0]
X_train = np.array([tr for tr in Data[:800, 0]])
# Y_train = Data[:800, 1]
Y_train = np.array([tr for tr in Data[:800, 1]])
X_test = Data[800:1000, 0]
Y_test = Data[800:1000, 1]

print(len(Y_train[0]))



# Размер мини-выборки
batch_size = 32
# Количество классов изображений
nb_classes = 150
# Количество эпох для обучения
nb_epoch = 25
# Размер изображений
img_rows, img_cols = 100, 100
# Количество каналов в изображении: RGB
img_channels = 3

# # Нормализуем данные
# X_train = X_train.astype('float32')
# X_test = X_test.astype('float32')
# X_train /= 255
# X_test /= 255

# Преобразуем метки в категории
# Y_train = np_utils.to_categorical(y_train, nb_classes)
# Y_test = np_utils.to_categorical(y_test, nb_classes)

# Создаем последовательную модель
model = Sequential()
# Первый сверточный слой
model.add(Conv2D(100, (3, 3), padding='same',
                        input_shape=(100, 100, 3), activation='relu'))
# Второй сверточный слой
model.add(Conv2D(100, (3, 3), activation='relu', padding='same'))
# Первый слой подвыборки
model.add(MaxPooling2D(pool_size=(2, 2)))
# Слой регуляризации Dropout
model.add(Dropout(0.25))

# Третий сверточный слой
model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
# Четвертый сверточный слой
model.add(Conv2D(64, (3, 3), activation='relu'))
# Второй слой подвыборки
model.add(MaxPooling2D(pool_size=(2, 2)))
# Слой регуляризации Dropout
model.add(Dropout(0.25))
# Слой преобразования данных из 2D представления в плоское
model.add(Flatten())
# Полносвязный слой для классификации
model.add(Dense(512, activation='relu'))
# Слой регуляризации Dropout
model.add(Dropout(0.5))
# Выходной полносвязный слой
model.add(Dense(nb_classes, activation='softmax'))

# Задаем параметры оптимизации
sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy',
              optimizer=sgd,
              metrics=['accuracy'])
# Обучаем модель
model.fit(X_train, Y_train,
              batch_size=batch_size,
              epochs=nb_epoch,
              validation_split=0.1,
              shuffle=True,
              verbose=2)



# Сохранение модели
model.save('model.lc')



