import numpy as np


def load():
    file_dataset = open("data.csv")
    dataset = file_dataset.readlines()
    file_dataset.close()

    Data = []
    k = 0
    for line in dataset[0:1000]:
        data = np.asfarray(line.split(', '))
        Input = data[0:10000] / 255
        Input = Input.reshape(100, 100)
        # Input = np.array([[[i[0], i[0], i[0]]] for i in Input]).reshape(100, 300)
        List = np.array([[[Input[i][j]] * 3 for i in range(100)] for j in range(100)])
        Input = List

        Output = list(data[10000:])
        Z = list(np.array([0 for i in range(150-len(Output))]))
        Output = np.array(([Output+Z][0]))

        Data.append([Input, Output])

        k += 1
        if k % 100 == 0:
            print('Загрузка данных', k / 10, '%')

    return np.array(Data)



def load_for_GUI():
    file_dataset = open("data.csv")
    dataset = file_dataset.readlines()
    file_dataset.close()

    Data = []
    k = 0
    for line in dataset[0:1000]:
        data = np.asfarray(line.split(', '))
        Input = data[0:10000] / 255
        Output = data[10000:]
        Data.append([Input, Output])

        k += 1
        if k % 100 == 0:
            print('Загрузка данных', k / 10, '%')

    return np.array(Data)


