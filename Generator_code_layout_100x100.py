import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import struct


class Generator:
    def __init__(self):
        self.layout = np.zeros(10000).reshape(100, 100)
        self.code = []


    def Create_Header(self):
        self.Height_Header = np.random.randint(15)+10
        self.Width_Header = 100

        self.layout[0:self.Height_Header, 0:self.Width_Header] = 100

        self.code.append('01')
        self.Create_Header_Content()
        self.code.append('10')


    def Create_Header_Content(self):
        self.Logo = np.random.randint(2)
        self.Nav_1 = np.random.randint(2)
        Header_Stack = np.array(['Nav1', 'Logo'])
        np.random.shuffle(Header_Stack)

        padding_main = 0

        for item in Header_Stack:
            if item == 'Logo' and self.Logo == 1:

                padding_top = np.random.randint(3) + 3
                padding_bottom = padding_top - 1
                padding_item = np.random.randint(30)
                self.layout[padding_top:self.Height_Header-padding_bottom, padding_main+padding_item:padding_main+padding_item+20] = 43
                self.code.append('01')
                self.code.append('10')

                padding_main += padding_item + 20

            if item == 'Nav1':
                padding_item = np.random.randint(30)
                width = np.random.randint(30)+30
                self.layout[3:self.Height_Header - 2, padding_main+padding_item:padding_main+padding_item + width] = 43
                self.code.append('01')
                self.code.append('10')

                padding_main += padding_item + width


    def Create_Content(self):
        self.code.append('01')
        self.Create_Aside()
        self.Create_Main()
        self.code.append('10')


    def Create_Aside(self):
        self.code.append('01')
        self.Position_Aside = np.random.randint(5)
        self.Width_Aside = np.random.randint(15)+15
        self.Height_Aside = np.random.randint(35)+30

        self.Content_Aside = np.random.randint(8)
        self.Height_Aside_Element = np.random.randint(5) + 4
        self.padding_Aside_Element = np.random.randint(2)+2
        self.padding_Aside_Element_rl = np.random.randint(2)+2


        if self.Position_Aside == 1 or self.Position_Aside == 3:
            self.layout[self.Height_Header:self.Height_Header+self.Height_Aside, 0:self.Width_Aside] = 25
            self.code.append('01')

            if self.Content_Aside != 0:
                self.Amount_Aside_Element = self.Height_Aside // (self.Height_Aside_Element+self.padding_Aside_Element)

                for i in range(self.Amount_Aside_Element):
                    self.code.append('01')
                    self.layout[self.Height_Header+i*(self.Height_Aside_Element+self.padding_Aside_Element)+self.padding_Aside_Element+1:self.Height_Header+i*(self.Height_Aside_Element+self.padding_Aside_Element)+self.padding_Aside_Element+self.Height_Aside_Element-1, 0+self.padding_Aside_Element_rl:self.Width_Aside-self.padding_Aside_Element_rl] = 72
                    self.code.append('10')

            self.code.append('10')

        elif self.Position_Aside == 2 or self.Position_Aside == 4:
            self.layout[self.Height_Header:self.Height_Header + self.Height_Aside, 100-self.Width_Aside:100] = 25
            self.code.append('01')

            if self.Content_Aside != 0:
                self.Amount_Aside_Element = self.Height_Aside // (self.Height_Aside_Element+self.padding_Aside_Element)

                for i in range(self.Amount_Aside_Element):
                    self.code.append('01')
                    self.layout[self.Height_Header+i*(self.Height_Aside_Element+self.padding_Aside_Element)+self.padding_Aside_Element+1:self.Height_Header+i*(self.Height_Aside_Element+self.padding_Aside_Element)+self.padding_Aside_Element+self.Height_Aside_Element-1, 100+self.padding_Aside_Element_rl-self.Width_Aside:100-self.padding_Aside_Element_rl] = 72
                    self.code.append('10')
            self.code.append('10')
        self.code.append('10')

    def Create_Main(self):
        self.code.append('01')
        Begin_X = 0
        Begin_Y = 0
        End_X = 0
        End_Y = 0
        self.padding_Main = np.random.randint(3)+2

        if self.Position_Aside == 0:
            Begin_Y = self.Height_Header + self.padding_Main
            Begin_X = self.padding_Main
            End_X = 100 - self.padding_Main
            End_Y = 100 - self.Height_Footer - self.padding_Main

        elif self.Position_Aside == 1 or self.Position_Aside == 3:
            Begin_Y = self.Height_Header + self.padding_Main
            Begin_X = self.padding_Main + self.Width_Aside
            End_X = 100 - self.padding_Main
            End_Y = 100 - self.Height_Footer - self.padding_Main

        elif self.Position_Aside == 2 or self.Position_Aside == 4:
            Begin_Y = self.Height_Header + self.padding_Main
            Begin_X = self.padding_Main
            End_X = 100 - self.padding_Main - self.Width_Aside
            End_Y = 100 - self.Height_Footer - self.padding_Main

        self.layout[Begin_Y:End_Y, Begin_X:End_X] = 52

        Amount_Section = np.random.randint(2) + 2
        padding_Section = np.random.randint(3)+2
        Height_Section = (End_Y - Begin_Y - padding_Section) // Amount_Section
        for i in range(Amount_Section):
            self.code.append('01')
            Amount_Article = np.random.randint(3)+1
            Proportions = self.Random_Proportions_Article(Amount_Article)
            padding_Article = np.random.randint(3) + 3
            Big_padding_Article = 0
            for j in range(Amount_Article):
                self.code.append('01')
                padding_Section_Y = self.Height_Header + self.padding_Main

                Width_Article = (End_X - Begin_X - padding_Article * (Amount_Article+1)) / 10 * Proportions[j]
                Width_Article = int(Width_Article)
                # Big_padding_Article += Width_Article

                self.layout[padding_Section_Y + i*Height_Section+padding_Section: padding_Section_Y + (i+1)*Height_Section,
                            Begin_X+padding_Article + Big_padding_Article:Begin_X+padding_Article+Width_Article + Big_padding_Article] = 92

                Big_padding_Article += Width_Article + padding_Article
                self.code.append('10')
            self.code.append('10')
        self.code.append('10')


    def Random_Proportions_Article(self, amount):
        List = []
        if amount == 1:
            Element_1 = 10
            List.append(Element_1)

        if amount == 2:
            Element_1 = np.random.randint(9)+1
            Element_2 = 10 - Element_1
            List.append(Element_1)
            List.append(Element_2)

        if amount == 3:
            Element_1 = np.random.randint(8)+1
            Element_2 = np.random.randint(10 - Element_1-1)+1
            Element_3 = 10 - Element_2 - Element_1
            List.append(Element_1)
            List.append(Element_2)
            List.append(Element_3)

        return List


    def Create_Footer(self):
        self.code.append('01')

        Width = 100

        self.layout[100-self.Height_Footer:100, 0:Width] = 120

        self.code.append('10')


    def show(self):
        # print(self.layout)
        plt.imshow(self.layout)
        plt.show()

    def Create(self):
        self.layout = np.zeros(10000).reshape(100, 100)
        self.code = []

        self.header = 1
        self.Logo = 0
        self.Nav_1 = 0
        self.Nav_2 = 0

        self.main_section = 1
        self.main = 0
        self.aside = 0

        self.footer = 1
        self.Height_Footer = np.random.randint(15) + 5

        self.Create_Header()
        self.Create_Content()
        self.Create_Footer()

        code = ''.join(self.code)

        data = np.array([self.layout.reshape(1, -1)[0], np.array(code)])

        return data



# G = Generator()
#
# dats = G.Create()
# #
# # print(dats)
# #
# # plt.imshow(dats[0].reshape(100, 100))
# # plt.show()
# #
# #
# #
# size = 100, 100
# arr = dats[0]
# print(arr)
# data = struct.pack('B'*len(arr), *[int(pixel) for pixel in arr])
# img = Image.frombuffer('L', size, data, 'raw', 'L', 0, 1)
# img.save('image.png')

