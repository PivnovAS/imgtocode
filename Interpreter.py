import loading_dataset as ld
import matplotlib.pyplot as plt


def BinCode_to_HtmlCode(data):
    code = []
    counter = 0
    for i in range(int(len(data)/2)):
        if data[i*2] == 0 and data[i*2 + 1] == 1:
            code.append('<p>'+'...' * counter + 'div class =""')
            counter += 1
        if data[i * 2] == 1 and data[i*2 + 1] == 0:
            counter -= 1
            code.append('<p>'+'...' * counter + '/div')

    # for line in code:
    #     print(line)
    #
    code = ''.join(code)
    return code


# BinCode_to_HtmlCode(ld.Data[5, 1])
# plt.imshow(ld.Data[5, 0].reshape(100, 100))
# plt.show()

